# atlantic
chat app made with node js

This is the official GitHub repository for Atlantic.

Atlantic uses socket-io to have real-time communication between connected clients.

Atlantic is currently in the process of migrating from SHA256 hashing to argon2.
